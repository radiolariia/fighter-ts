import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../../constants/interfaces/Fighter';
import { FighterExtended } from '../../constants/interfaces/FighterExtended';

class FighterService {
  public async getFighters() : Promise<Fighter[]> {
    try {
      const endpoint : string = 'fighters.json';
      const apiResult : Fighter[] = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  public async getFighterDetails(id: string) : Promise<FighterExtended> {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult : FighterExtended = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
