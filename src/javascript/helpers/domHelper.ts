import { DOMElement } from '../../constants/interfaces/DOMElement';

export function createElement(
  { 
    tagName, 
    className, 
    attributes = {}
  } : DOMElement ) : HTMLElement {
  const element : HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames : string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
