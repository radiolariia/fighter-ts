import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { Fighter } from '../constants/interfaces/Fighter';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement : HTMLElement = document.getElementById('root');
  static loadingElement : HTMLElement = document.getElementById('loading-overlay');

  public async startApp(): Promise<void> {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters : Fighter[]= await fighterService.getFighters();
      const fightersElement : HTMLElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
