import { createElement } from '../helpers/domHelper';
import { FighterExtended } from '../../constants/interfaces/FighterExtended';
import { Position } from '../../constants/enums/Position';

export function createFighterPreview(fighter : FighterExtended, position : Position) : HTMLElement {
  let positionClassName  : string;
  switch(position) {
    case Position.right: 
      positionClassName = 'fighter-preview___right';
      break;
    case Position.left:
      positionClassName = 'fighter-preview___left';
      break;
  }
  const fighterElement : HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if(fighter) {
    const fighterImage : HTMLElement = createFighterImage(fighter);
    const fighterInfo : HTMLElement = createDetailsCard(fighter);
    fighterElement.append(fighterImage, fighterInfo);
  };

  return fighterElement;
}

export function createFighterImage(fighter : FighterExtended) : HTMLElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement : HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createDetailsCard(fighter : FighterExtended) : HTMLElement {
  const info : HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });

  const { source, _id, ...fighterDetails} = fighter;
  for(let property in fighterDetails) {
    const element : HTMLElement = createElement({ tagName: 'p', 
      className: `fighter-preview__${property}`
    });
    element.innerText = property + ' ' + fighterDetails[property];
    info.append(element);
  }

  return info;
}