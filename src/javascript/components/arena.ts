import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { SelectedFighters } from '../../constants/types/SelectedFighters';
import { FighterExtended } from '../../constants/interfaces/FighterExtended';
import { Position } from '../../constants/enums/Position';

export async function renderArena(selectedFighters : SelectedFighters) : Promise<void> {
  const root : HTMLElement = document.getElementById('root');
  const arena : HTMLElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);
  
  try {
    const winner : FighterExtended = await fight(selectedFighters);
    showWinnerModal(winner);
  } catch (error) {
    throw error;
  }
}

function createArena(selectedFighters : SelectedFighters) : HTMLElement {
  const arena : HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators : HTMLElement = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
  const fighters : HTMLElement = createFighters(selectedFighters[0], selectedFighters[1]);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter : FighterExtended, rightFighter : FighterExtended) : HTMLElement {
  const healthIndicators : HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign : HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator : HTMLElement = createHealthIndicator(leftFighter, Position.left);
  const rightFighterIndicator : HTMLElement = createHealthIndicator(rightFighter, Position.right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter : FighterExtended, position : Position) : HTMLElement {
  const { name } = fighter;
  const container : HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName : HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator : HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar : HTMLElement = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter : FighterExtended, secondFighter : FighterExtended) : HTMLElement {
  const battleField : HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement : HTMLElement = createFighter(firstFighter, Position.left);
  const secondFighterElement : HTMLElement = createFighter(secondFighter, Position.right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter : FighterExtended, position : Position) : HTMLElement {
  const imgElement : HTMLElement = createFighterImage(fighter);
  let positionClassName  : string;
  switch(position) {
    case Position.right: 
      positionClassName = 'arena___right-fighter';
      break;
    case Position.left:
      positionClassName = 'arena___left-fighter';
      break;
  }
  const fighterElement : HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}