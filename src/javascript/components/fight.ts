import { controls } from '../../constants/controls';
import { SelectedFighters } from '../../constants/types/SelectedFighters';
import { FighterExtended } from '../../constants/interfaces/FighterExtended';
import { Player } from '../../constants/interfaces/Player';

const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoAttack,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

export async function fight(selectedFighters : SelectedFighters) : Promise<FighterExtended> {
  const [firstFighter, secondFighter] = selectedFighters;
  const playerOne : Player = createPlayer(firstFighter);
  const playerTwo : Player = createPlayer(secondFighter);
  
  return new Promise(resolve => {
    // resolve the promise with the winner when fight is over
    const pressedKeys : Set<string> = new Set();

    document.addEventListener('keydown', (e) => {
      pressedKeys.add(e.code);
      
      handlePunches(playerOne, playerTwo, pressedKeys);

      if (playerOne.currentHealth <= 0 || playerTwo.currentHealth <= 0) {
        const winner = playerOne.currentHealth <= 0 ? secondFighter : firstFighter;
        resolve(winner);
      };
    });
    
    document.addEventListener('keyup', (e) => {
      //prevent sticky keys
      pressedKeys.delete(e.code);
    });
  });
}
function createPlayer(fighter : FighterExtended) : Player {
  return {
    ...fighter,
    currentHealth: fighter.health,
    lastCriticalHit: new Date(0),
    setCriticalHitTimer() {
      this.lastCriticalHit = new Date();
    }
  }
}
function handlePunches(firstFighter : Player, secondFighter : Player, pressedKeys : Set<string>) : void {
  const leftHealthIndicator : HTMLElement = document.getElementById('left-fighter-indicator');
  const rightHealthIndicator : HTMLElement = document.getElementById('right-fighter-indicator');
  
  switch(true) {
    case pressedKeys.has(PlayerOneAttack): {
      controlFighterAttack(firstFighter, secondFighter, rightHealthIndicator, pressedKeys);
    };
    break;
    case pressedKeys.has(PlayerTwoAttack): {
      controlFighterAttack(secondFighter, firstFighter, leftHealthIndicator, pressedKeys);
    };
    break;
    case PlayerOneCriticalHitCombination.every(key => pressedKeys.has(key)): {
      controlFighterCriticalAttack(firstFighter, secondFighter, rightHealthIndicator);
    };
    break;
    case PlayerTwoCriticalHitCombination.every(key => pressedKeys.has(key)): {
      controlFighterCriticalAttack(secondFighter, firstFighter, leftHealthIndicator);
    };
    break;
  };
}
function controlFighterAttack(attacker : Player, defender : Player, healthIndicator : HTMLElement, pressedKeys : Set<string>) : void {
  if (isBlocked(pressedKeys)) return
  defender.currentHealth -= getDamage(attacker, defender);
    updateHealthIndicator(defender, healthIndicator);
}

function controlFighterCriticalAttack(attacker : Player, defender : Player, healthIndicator : HTMLElement) : void {
  if (isCriticalHitInTime(attacker)) {
    defender.currentHealth -= attacker.attack * 2;
    updateHealthIndicator(defender, healthIndicator);
    
    attacker.setCriticalHitTimer();
  }
}
function isBlocked(pressedKeys : Set<string>) : boolean {
  return pressedKeys.has(PlayerOneBlock) || pressedKeys.has(PlayerTwoBlock);
}
function isCriticalHitInTime(attacker : Player) : boolean {
  const interval : number = (new Date().getTime() - attacker.lastCriticalHit.getTime()) / 1000;
  return interval > 10;
}
function updateHealthIndicator(defender : Player, indicator : HTMLElement) : void {
  const indicatorWidth : number = Math.max(0, (defender.currentHealth / defender.health) * 100);
  indicator.style.width = indicatorWidth + '%';
} 
export function getDamage(attacker : Player, defender : Player) : number{
  // return damage
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender))
}
export function getHitPower(fighter : Player) : number {
  // return hit power
  const randomNumber : number = getRandomNum(1,2);
  const attack : number = fighter.attack;
  return  randomNumber * attack;
}
export function getBlockPower(fighter : Player) : number {
  // return block power
  const randomNumber : number = getRandomNum(1,2);
  const defense : number = fighter.defense;
  return randomNumber * defense;
}
function getRandomNum(min : number, max : number) : number {
  return Math.random() * (max - min) + min;
}