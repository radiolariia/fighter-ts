import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { Position } from '../../constants/enums/Position';
export async function renderArena(selectedFighters) {
    const root = document.getElementById('root');
    const arena = createArena(selectedFighters);
    root.innerHTML = '';
    root.append(arena);
    try {
        const winner = await fight(selectedFighters);
        showWinnerModal(winner);
    }
    catch (error) {
        throw error;
    }
}
function createArena(selectedFighters) {
    const arena = createElement({ tagName: 'div', className: 'arena___root' });
    const healthIndicators = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
    const fighters = createFighters(selectedFighters[0], selectedFighters[1]);
    arena.append(healthIndicators, fighters);
    return arena;
}
function createHealthIndicators(leftFighter, rightFighter) {
    const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
    const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
    const leftFighterIndicator = createHealthIndicator(leftFighter, Position.left);
    const rightFighterIndicator = createHealthIndicator(rightFighter, Position.right);
    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
}
function createHealthIndicator(fighter, position) {
    const { name } = fighter;
    const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
    const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
    const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
    const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` } });
    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);
    return container;
}
function createFighters(firstFighter, secondFighter) {
    const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
    const firstFighterElement = createFighter(firstFighter, Position.left);
    const secondFighterElement = createFighter(secondFighter, Position.right);
    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
}
function createFighter(fighter, position) {
    const imgElement = createFighterImage(fighter);
    let positionClassName;
    switch (position) {
        case Position.right:
            positionClassName = 'arena___right-fighter';
            break;
        case Position.left:
            positionClassName = 'arena___left-fighter';
            break;
    }
    const fighterElement = createElement({
        tagName: 'div',
        className: `arena___fighter ${positionClassName}`,
    });
    fighterElement.append(imgElement);
    return fighterElement;
}
