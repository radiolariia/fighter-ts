import { createElement } from '../helpers/domHelper';
import { Position } from '../../constants/enums/Position';
export function createFighterPreview(fighter, position) {
    let positionClassName;
    switch (position) {
        case Position.right:
            positionClassName = 'fighter-preview___right';
            break;
        case Position.left:
            positionClassName = 'fighter-preview___left';
            break;
    }
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });
    if (fighter) {
        const fighterImage = createFighterImage(fighter);
        const fighterInfo = createDetailsCard(fighter);
        fighterElement.append(fighterImage, fighterInfo);
    }
    ;
    return fighterElement;
}
export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });
    return imgElement;
}
function createDetailsCard(fighter) {
    const info = createElement({
        tagName: 'div',
        className: 'fighter-preview___info',
    });
    const { source, _id, ...fighterDetails } = fighter;
    for (let property in fighterDetails) {
        const element = createElement({ tagName: 'p',
            className: `fighter-preview__${property}`
        });
        element.innerText = property + ' ' + fighterDetails[property];
        info.append(element);
    }
    return info;
}
