import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { SelectedFighters } from '../../constants/types/SelectedFighters';
import { FighterExtended } from '../../constants/interfaces/FighterExtended';
import { Position } from '../../constants/enums/Position';

const versusImg : string = '../../../resources/versus.png';

export function createFightersSelector() : Function {
  let selectedFighters : FighterExtended[] = [];

  return async (event : Event, fighterId : string) : Promise<void> => {
    const fighter : FighterExtended = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter : FighterExtended = playerOne ?? fighter;
    const secondFighter : FighterExtended = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap : Map<string, FighterExtended> = new Map();

export async function getFighterInfo(fighterId : string) : Promise<FighterExtended> {
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId);
  };
  try {
    const fighterDetails : FighterExtended = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterDetails);
    return fighterDetails;
  } catch (error) {
    throw error;
  };
}

function renderSelectedFighters(selectedFighters : SelectedFighters) : void {
  const fightersPreview : Element = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview : HTMLElement = createFighterPreview(playerOne, Position.left);
  const secondPreview : HTMLElement = createFighterPreview(playerTwo, Position.right);
  const versusBlock : HTMLElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters : SelectedFighters) : HTMLElement {
  const canStartFight : boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container : HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image : HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn : string = canStartFight ? '' : 'disabled';
  const fightBtn : HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image);
  container.append(fightBtn);

  return container;
}

function startFight(selectedFighters : SelectedFighters) : void {
  renderArena(selectedFighters);
}