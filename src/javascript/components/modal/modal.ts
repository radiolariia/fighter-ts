import { createElement } from '../../helpers/domHelper';
import { ModalObj } from '../../../constants/interfaces/ModalObj';

export function showModal({ 
  title, 
  bodyElement, 
  onClose = () => {} 
} : ModalObj ) : void {
  const root : HTMLElement = getModalContainer();
  const modal : HTMLElement = createModal({ title, bodyElement, onClose }); 
  
  root.append(modal);
}

function getModalContainer() : HTMLElement {
  return document.getElementById('root');
}

function createModal(obj : { title : string, bodyElement: HTMLElement, onClose : Function }) : HTMLElement {
  const layer : HTMLElement = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer : HTMLElement = createElement({ tagName: 'div', className: 'modal-root' });
  const header : HTMLElement = createHeader(obj.title, obj.onClose);

  modalContainer.append(header, obj.bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title : string, onClose : Function) : HTMLElement {
  const headerElement : HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement : HTMLElement = createElement({ tagName: 'span' });
  const closeButton : HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal() : void {
  const modal : Element = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
