import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';
import { FighterExtended } from '../../../constants/interfaces/FighterExtended';
import { ModalObj } from '../../../constants/interfaces/ModalObj';

export function showWinnerModal(fighter : FighterExtended) : void {
  const imageElement : HTMLElement = createFighterImage(fighter);
  const modalElement : ModalObj = {
    title: `${fighter.name.toUpperCase()} WINS!`,
    bodyElement: imageElement
  };
  // call showModal function
  showModal(modalElement);  
}