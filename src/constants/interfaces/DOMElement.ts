export interface DOMElement {
    tagName : string;
    className? : string; 
    attributes? : object;
}