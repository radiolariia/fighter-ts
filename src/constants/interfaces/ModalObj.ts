export interface ModalObj {
    title : string; 
    bodyElement : HTMLElement;
    onClose? : Function; 
}