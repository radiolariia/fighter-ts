import { Fighter } from './Fighter';
export interface FighterExtended extends Fighter {
    attack: number;
    defense:  number;
    health:  number;
}