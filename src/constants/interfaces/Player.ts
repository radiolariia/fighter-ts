import { FighterExtended } from './FighterExtended';

export interface Player extends FighterExtended {
    currentHealth: number;
    lastCriticalHit: Date;
    setCriticalHitTimer() : void;
}