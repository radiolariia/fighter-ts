import { FighterExtended } from '../interfaces/FighterExtended';

export type SelectedFighters = FighterExtended[];
