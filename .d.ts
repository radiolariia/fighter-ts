import PngImg from "png-img";

declare module "*.png" {
    const value : PngImg;
    export default value;
}