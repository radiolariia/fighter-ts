# Street Fighter

Small demo game for practice in JS ES6, Typescript, Webpack.

`npm install`  

`npm run tsc build`  

`npm run build`  

`npm run start`  

open http://localhost:8080/

## Tutorial
------
### First player controls
A - punch  
D - block  
Q + W + E - critical hit that is available once in 10 seconds  

### Second player controls
J - punch  
L - block  
U + I + O - critical hit that is available once in 10 seconds  

------
_“That's all Folks!”_ <sup>🐷</sup>